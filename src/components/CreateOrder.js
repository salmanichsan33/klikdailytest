import React, { useState, useEffect } from "react";
import axios from 'axios';

import { Container, Card, CardHeader, CardContent, Grid, TextField, CardActions, Button } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";

const CreateOrder = props => {
    const [name, setName] = useState({});
    const dc = [
        {name: 'DC Tangerang'},
        {name: 'DC Cikarang'}
    ];
    const pm = [
        {name: 'Cash H+1'},
        {name: 'Cash H+3'},
        {name: 'Cash H+7'},
        {name: 'Transfer H+1'},
        {name: 'Transfer H+3'},
        {name: 'Transfer H+7'}
        
    ];
    const [products, setProducts] = useState([
        {id: 1, name: 'Naga Fullcream Milk 1L', units:[{id:1,name:'karton', price: 20000},{id:2,name:'pak', price: 10000}, {id:3,name:'pcs', price:5000}]},
        {id: 2, name: 'Kuda Fullcream Milk 1L', units:[{id:1,name:'karton', price: 18000},{id:2,name:'pak', price: 8000}]},
        {id: 3, name: 'Osron Sirup 1L', units:[{id:1,name:'karton', price: 10000},{id:3,name:'pcs', price:2000}]},
    ])
    const notdata = [{name:'Data not available'}];
    const [valuename, setValuename] = useState(null);
    const [valuedc, setValuedc] = useState(null);
    const [valuepm, setValuepm] = useState(null);
    const [valuedate, setValuedate] = useState(null);
    const [valuenote, setValuenote] = useState(null);

    const [valueproductname, setValueproductname] = useState(null);
    const [valueproductunit, setValueproductunit] = useState(null);
    const [valueproductqty, setValueproductqty] = useState(null);
    const [totalprice, setTotalprice] = useState(null);
    const [totalnettprice, setTotalnettprice] = useState(0);
    const [orderlist, setOrderlist] = useState([{nama: [], unit: [], qty: 0, total:0}])
    // berikut http://dummy.restapiexample.com/api/v1/employees.
    const getData = () => {
        return axios.get(`https://jsonplaceholder.typicode.com/users`)
                    .then(res => {
                        const persons = res.data;                        
                        setName(persons);
                    })
    }

    const handleInputChangename = (e, index, v) => {
        const { name, value } = v;
        
        const list = [...orderlist];
        list[index]["nama"] = v;        
        setOrderlist(list);
    };

    const handleInputChangeunit = (e, index, v) => {
        const { name, value } = v;
        
        const list = [...orderlist];
        list[index]["unit"] = v;        
        setOrderlist(list);
    };

    const handleInputChange = (e, index) => {
        const { name, value } = e.target;        
        const list = [...orderlist];
        list[index][name] = value;        
        setOrderlist(list);
    };

    const handleInputChangetotal = (e, index) => {
        // const { name, value } = e.target;        
        const list = [...orderlist];
        list[index]["total"] = e;        
        setOrderlist(list);
    };

    const handleRemoveClick = index => {
        const list = [...orderlist];        
        list.splice(index, 1);
        
        setOrderlist(list);
    };

    const handleAddClick = (a,id) => {          
            // products.filter(person => person.id == a && person.units.id == id).map(filteredPerson => (
            //     // setProducts(filteredPerson.id[a].units)            
            //     console.log(filteredPerson)
            // ));
            
            setOrderlist([...orderlist, { nama: [], unit: [], qty: null, total:null} ]);
        
      };

    
    useEffect(() => {                  
        getData();  
        console.log(products)
      }, []);
       
    
    return (
        <Container style={{marginTop:'10pt'}}>
            <Card>
                <CardHeader title="Create Order"/>                    
                <CardContent>
                    <Grid container>
                        <Grid item sm={3}>
                            <h5>Detail</h5>
                        </Grid>
                        <Grid item container sm={9}>
                            <Grid item sm={12}>
                                <h5 >Name</h5>
                                <Autocomplete
                                    id="combo-box-demo"
                                    value={valuename}
                                    onChange={(event, newValue) => {
                                        setValuename(newValue);                                        
                                    }}
                                    options={name}
                                    getOptionLabel={(option) => option.name}
                                    style={{ width: 300 }}
                                    renderInput={(params) => <TextField {...params} />}
                                />
                            </Grid><br></br>
                            <Grid item sm={12}>
                                <h5 >Distribution Center</h5>
                                <Autocomplete
                                    id="combo-box-demo"
                                    value={valuedc}
                                    onChange={(event, newValue) => {
                                        setValuedc(newValue);
                                    }}
                                    options={valuename !== null ? dc : notdata}
                                    disabled = {valuename !== null ? false : true}
                                    getOptionLabel={(option) => option.name}
                                    style={{ width: 300 }}
                                    renderInput={(params) => <TextField {...params} label={valuename !== null ? "" : "Data Not Available"} />}
                                />
                            </Grid>
                            <Grid container item style={valuedc == null ? {display:'none'} : {}}>
                                <Grid item sm={6}>
                                    <h5>Payment Type</h5>
                                    <Autocomplete
                                        id="combo-box-demo"
                                        onChange={(event, newValue) => {
                                            setValuepm(newValue);
                                        }}
                                        value={valuepm}
                                        options={pm}
                                        getOptionLabel={(option) => option.name}
                                        style={{ width: 300 }}
                                        renderInput={(params) => <TextField {...params}  />}
                                    />
                                </Grid>
                                <Grid item sm={6}>
                                    <h5 >Expired Date</h5>
                                    <TextField
                                        id="date"
                                        onChange={(event, newValue) => {
                                            setValuedate(newValue);
                                        }}
                                        value={valuedate}
                                        type="date"                                                                        
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </Grid>
                                <Grid item sm={12}>
                                    <h5>Note</h5>
                                    <TextField
                                        id="outlined-multiline-static"
                                        inputValue={valuenote}
                                            onInputChange={(event, newInputValue) => {
                                            setValuenote(newInputValue);
                                        }}
                                        multiline
                                        rows={4}
                                        value={valuenote}
                                        variant="outlined"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>                        
                        <Grid item sm={3} style={valuedc == null ? {display:'none'} : {}}>
                            <h5>Products</h5>
                        </Grid>
                        
                            <Grid item container sm={9} style={valuedc == null ? {display:'none'} : {}}>
                            {orderlist.map((x, i) => {
                                return(
                                <> 
                                
                                <Grid item sm={6}>
                                    <h5 >Product</h5>
                                    <Autocomplete
                                        id="combo-box-demo"
                                        name="name"                                       
                                        value={x.name}
                                        onChange={(e,value) => handleInputChangename(e, i, value)}
                                        options={products}
                                        getOptionLabel={(option) => option.name}
                                        style={{ width: 300 }}
                                        renderInput={(params) => <TextField {...params} />}
                                    />
                                </Grid>
                                <Grid item sm={6}>
                                    <h5 >Unit</h5>
                                    <Autocomplete
                                        id="combo-box-demo"
                                        
                                        name="unit"                                       
                                        value={x.nama.units}
                                        onChange={(e, v) => handleInputChangeunit(e, i, v)}
                                        options={x.nama !== null ? x.nama.units : notdata}
                                        disabled = {x.nama !== null ? false : true}
                                        getOptionLabel={(option) => option.name}
                                        style={{ width: 300 }}
                                        renderInput={(params) => <TextField {...params} label={valuename !== null ? "" : "Data Not Available"} />}
                                    />
                                </Grid>
                                <Grid item sm={3}>
                                    <h5>Quantity</h5>
                                    <TextField                                                                                                                     
                                        name="qty"
                                        value={x.qty}
                                        onChange={e => (handleInputChange(e, i), handleInputChangetotal((x.qty*1)*(x.unit.price*1),i))}                                        
                                        variant="outlined"                                
                                    />
                                </Grid>
                                <Grid item sm={3}>
                                    <h5>Price</h5>
                                    <TextField                                     
                                        value={x.unit !== null ? x.unit.price : 0 } 
                                        variant="outlined"                                
                                    />
                                </Grid>
                                <Grid item sm={6}>
                                    <h5>Total Price</h5>
                                    <TextField     
                                        name="total"  
                                        disabled                              
                                        value={(x.qty*1)*(x.unit.price*1)}
                                        onChange={e => handleInputChange(e, i)}                                        
                                        variant="outlined"                                
                                    />
                                </Grid>
                                <Grid item sm={6}>{orderlist.length !== 1 && <button
                                className="mr10"
                                onClick={() => handleRemoveClick(i)}>Remove</button>}                                                               </Grid>
                                <Grid item sm={6}>
                                    ----------------------------------
                                    <h5>Total nett price {x.total}</h5>
                                </Grid>
                                {orderlist.length - 1 === i && <button onClick={()=>handleAddClick(x.nama.id,x.unit.id)}>Add</button>}
                                </>
                                )
                            })};
                            {/* <div style={{ marginTop: 20 }}>{JSON.stringify(orderlist)}</div> */}
                            </Grid>
                            
                    </Grid>
                </CardContent>
                <CardActions style={{justifyContent:'flex-end'}}>
                    <Button size="small" color="primary" variant="outlined">
                    Cancel
                    </Button>
                    <Button size="small" color="primary" variant="outlined">
                    Confirm
                    </Button>
                </CardActions>

            </Card>
        </Container>
    );
};

export default CreateOrder;
