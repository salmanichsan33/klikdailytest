import React from 'react';
import logo from './logo.svg';
import './App.css';
import BaseLayout from './layouts/BaseLayout'

function App() {
  return (
    <BaseLayout/>
  );
}

export default App;
